// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"  //!!! Turn Camera Component
#include "SnakeBase.h"
#include "ExampleActor.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT ("PawnCamera"));
	RootComponent = PawnCamera;


}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator (-90,0,0));			// TurnCamera
	
	CreateSnakeActor();
	CreateExampleActor();
	

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);



}

void APlayerPawnBase::CreateSnakeActor()         //Create Snake 2 part
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());



}
void APlayerPawnBase::CreateExampleActor()       // Create My Actor
{
	Example = GetWorld()->SpawnActor<AExampleActor>(ExampleActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDiraction != EMovementDiraction::DOWN)
		{
			SnakeActor->LastMoveDiraction = EMovementDiraction::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDiraction != EMovementDiraction::UP)
		{
			SnakeActor->LastMoveDiraction = EMovementDiraction::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDiraction != EMovementDiraction::LEFT)
		{
			SnakeActor->LastMoveDiraction = EMovementDiraction::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDiraction != EMovementDiraction::RIGHT)
		{
			SnakeActor->LastMoveDiraction = EMovementDiraction::LEFT;
		}
	}


	
	
}



