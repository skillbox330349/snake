﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExampleActor.generated.h"


UENUM(BlueprintType)  //        Перечисления с типом БП
enum class EExample : uint8
{
	E_RED	UMETA(DisplayName = "RED"),
	E_GREEN	UMETA(DisplayName = "GREEN"),
	E_BLUE	UMETA(DisplayName = "BLUE")
};

USTRUCT(BlueprintType)			//            Структуры
struct FMyData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)  // Полный доступ из БП
	EExample EnumValue;
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
	int32 IntValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString StringValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* ActorPtr;

	FMyData()
	{
		EnumValue	= EExample::E_BLUE;  // инициализация полей
		IntValue	= 10;
		StringValue = TEXT("My Text from VisualStudio");	// запись строки в переменную
		ActorPtr	= nullptr;			// Инициализация указателя
	}

};


UCLASS()
class SNAKE_API AExampleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExampleActor();

	UPROPERTY(BlueprintReadOnly)  // Доступность переменной в БП
	EExample ExamlpeUnumValue;

	UPROPERTY (BlueprintReadWrite)// Переменная с редактированием из БП
	FMyData ExampleStructValue;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
