// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include"SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;

	LastMoveDiraction = EMovementDiraction::DOWN;
	MovementSpeed = 10.f;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		//SnakeElements.Num()*ElementSize;
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform = FTransform(NewLocation);

		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);

		NewSnakeElement->SnakeOwner = this;
		//NewSnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		int32 ElemIndex;
		//SnakeElements.Add(NewSnakeElement);
		ElemIndex = SnakeElements.Add(NewSnakeElement); //Metod ADD return index array
		

		if (ElemIndex == 0)  // HEAD Snake
		{
			NewSnakeElement->SetFirstElementType();
			//NewSnakeElement->MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	MovementSpeed = ElementSize;
	

	switch (LastMoveDiraction)
	{
	case EMovementDiraction::UP:
		MovementVector.X += MovementSpeed;
		break;
	case EMovementDiraction::DOWN:
		MovementVector.X -= MovementSpeed;
		break;
	case EMovementDiraction::LEFT:
		MovementVector.Y += MovementSpeed;
		break;
	case EMovementDiraction::RIGHT:
		MovementVector.Y -= MovementSpeed;
		break;
	}
	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();



	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CarrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CarrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverLap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		IInteractable* InteractableInterface = Cast <IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}
